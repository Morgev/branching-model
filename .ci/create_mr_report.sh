#!/bin/bash

# Appel : ./create_mr_report.sh 1.4-stable

TARGET_BRANCH=$1
HOST_API="https://gitlab.com/api/v4/projects"
PRIVATE_TOKEN="${PRIVATE_TOKEN}"
PROJECT_ID="${CI_PROJECT_ID}"
COMMIT_SHORT_SHA="${CI_COMMIT_SHORT_SHA}"
USER_ID="${USER_ID}"

exit_code=0

# Check nb parameters
if [ "$#" -ne 1 ]; then
  echo "[ERREUR] Nombre d'arguments non suffisant (\$1:branch_cible) !"
  exit 1
fi


# Création Branche temporaire
echo "[INFO ] Création de la branche temporaire 'cherry-pick-${TARGET_BRANCH}-${COMMIT_SHORT_SHA}' à partir de la branche '${TARGET_BRANCH}'"

http_response=$(curl -s -o response.txt -w %{http_code} --request POST --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "${HOST_API}/${PROJECT_ID}/repository/branches?branch=cherry-pick-${TARGET_BRANCH}-${COMMIT_SHORT_SHA}&ref=${TARGET_BRANCH}")
message=$(jq -j '.message' response.txt)

if [ $http_response != "201" ]; then
    echo "[ERROR] $message ($http_response)"
    exit_code=1
fi

# Cherry-Pick sur la branche temp
echo "[INFO ] Cherry-pick du commit '${COMMIT_SHORT_SHA}' sur la branche 'cherry-pick-${TARGET_BRANCH}-${COMMIT_SHORT_SHA}'"
http_response=$(curl -s -o response.txt -w %{http_code} --request POST --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" --form "branch=cherry-pick-${TARGET_BRANCH}-${COMMIT_SHORT_SHA}" "${HOST_API}/${PROJECT_ID}/repository/commits/${COMMIT_SHORT_SHA}/cherry_pick")
message=$(jq -j '.message' response.txt)

if [ $http_response != "201" ]; then
    echo "[ERROR] $message ($http_response)"
    exit_code=1
fi

# Merge Request
echo "[INFO ] Création de la merge request"
http_response=$(curl -s -o response.txt -w %{http_code} --request POST "${HOST_API}/${PROJECT_ID}/merge_requests" --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" --header "Content-Type: application/json" --data "{\"source_branch\": \"cherry-pick-${TARGET_BRANCH}-${COMMIT_SHORT_SHA}\",\"target_branch\": \"${TARGET_BRANCH}\",\"remove_source_branch\": true,\"squash\": true,\"labels\":\"report\",\"title\": \":twisted_rightwards_arrows: ${COMMIT_SHORT_SHA} depuis master vers ${TARGET_BRANCH}\",\"assignee_id\":\"${USER_ID}\"}")

if [ $http_response != "201" ]; then
    message=$(jq -j '.message | .[0]' response.txt)
    echo "[ERROR] $message ($http_response)"
    exit_code=1
else
    web_url=$(jq '.web_url' response.txt | sed 's/"//g')
    echo "[INFO ] MR créée : $web_url"
fi

rm ./response.txt # Nettoyage du fichier temporaire contenant les retours de l'API
exit $exit_code
